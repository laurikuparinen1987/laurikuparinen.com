<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "KTP Database";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>KTP Database</h1>
        <h2>Football club statistics collection (2006-)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>I have supported <a href="http://www.fcktp.fi" target="_blank">Football Club KTP</a> from the year 2000. I have always been fascinated by the club's history but as it's usually the case in Finnish football, it's really difficult to find old results and even competition tables. Even more difficult is to find out how my club played in the 1973 Finnish Cup, for example.</p>
            <p>I started out this project as a small Head to Head comparison spreadsheet in the year 2006 to find out, for example, how many times KTP has met with HJK and how many times we have actually won them. After years of research and studying old football books, web sites etc., the database now contains Head to Head statistics from the 1947 season of several of the most commonly met opponents. I have also created a Season to Season spreasheet covering every season of KTP under the SPL (Finnish FA) competitions. There's still a lot of missing data and probably quite a few errors there so if you spot any, let me know.   
            </p>
            <p>Some of you might be aware that KTP has had a lot different names over the years: KTP (1927-1998), Kotkan TP (1999-2000), FC KooTeePee (2001-2013) and now FC KTP. During KooTeePee years, there was also a club called KTP and at one point I only covered their matches but after a merger between FC KooTeePee and KTP in 2013 I decided to merge the database as well and cover the "top" football of KTP. This means that seasons from 2001 to 2013 are FC KooTeePee matches. Simple, right? 
            </p>
            <p>The databases are in ods file format (LibreOffice Calc, the free & open source Excel alternative) for your download pleasure below. These files should open in Excel as well but some of the styling in spreadsheet may not display correctly (I would also like to point out that the database is in Finnish). <br /><br/>Here are the links: <br /><br />

            <a href="projects/ktp/Head_to_Head_v3_1.ods" target="_blank">Head to Head statistics</a><br />
            <a href="projects/ktp/Kausi_kaudelta_v2_1.ods" target="_blank">Season to Season statistics</a>
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>