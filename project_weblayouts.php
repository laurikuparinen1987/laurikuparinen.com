<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Web Layouts";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Site Layouts</h1>
        <h2>Collection of web site layouts I've done over the years</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>I have done A LOT of web layout designs over the years. Problem? Most of the sites I have designed and coded have vanished into the oblivion and in way too many cases I have no backups of my old work. However, I do have some layouts and sites saved so I can show them to you.</p>
            <p>What you see here is a collection of finished sites and a collection of website layouts I did for the clients. In most cases, the layouts were never used as the site projects never got of the ground or the negotiations failed. So what I was left with was a collection of 'useless' layouts no one could see. I'm still trying to find more of my old layouts to showcase in here; I have done some real gems I really would like to share with people...</p>
        </div>

        <div class="image-container">
            <img src="projects/layouts/blackland-upnrunning-preview.png" alt="Blackland Games website">
            <img src="projects/layouts/swing2-preview.jpg" alt="Swing website layout">
            <img src="projects/layouts/tifcon2-preview.jpg" alt="Tifcon website layout">
            <img src="projects/layouts/villivarvas-preview.jpg" alt="Villivarvas website layout">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>