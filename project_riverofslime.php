<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "River of Slime";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>River of Slime</h1>
        <h2>Unity3D scene (2012)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>"Slime! It's a River of Slime!" - Short area based on a scene in movie "Ghostbusters II" where the Ghostbusters descend underground and discover a river of slime flowing underneath the city of New York. I was always fascinated by this area and I also had nightmares as a kid about this place. So it was a perfect location to recreate! Modeled in 3ds Max, textured in Photoshop and everything was put together in Unity.</p>
        </div>

        <div class="image-container">
            <img src="projects/gbii/original-preview.png" alt="Original">
            <img src="projects/gbii/slime1-preview.jpg" alt="New">
            <img src="projects/gbii/slime2-preview.jpg" alt="New">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>