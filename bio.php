<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Bio";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Bio</h1>
        <h2>A brief history of me</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>I was born in January of 1987, during a horrible snowstrom (or so my father says) in Kotka, Finland. Since the very early days I have always been really into computers, games and techology. I started BASIC programming on Commodore 64 in the year 1998, when the machine was already ancient! Since then I have done a lot of small web and game projects by myself using PC and it's programming languages like QBasic and Visual Basic. Despite my longish history with programming, I still heavily prefer the design and artistic side to projects. I have been using Windows OS'es from the version 3.1 to the latest (but certainly not the greatest) version 10. I have also been using MS DOS 6 and Amiga Workbench 1.3 back in the 90's. <br /><br />Today, I live in Helsinki, Finland and as my main OS I use Linux while I try to gather others to my open source crusade (my friends have had enough of my Linux propaganda, though).<br /><br />
            <h3>Education</h3>
                        <p>
            8/2011 – 6/2015	Kymenlaakso University of Applied Sciences, Kouvola, viestinnän ko., digital media (medianomi), Degree Programme in Media Communication<br />
            8/2006 - 12/2008	Vocational school of Etelä-Kymenlaakso (Ekami), Degree in Information Technology<br />
            8/2003 – 2/2006	Upper secondary school of Karhula (Kotka)
                        </p>
            <h3>Honours</h3>
            <p>
            2019 - Menestystarinat - Employee of the year<br />
            2015 - Kymenlaakso University of Applied Sciences - Stipend, on my work on Wolf Track<br />
            2014 - Kymenlaakso University of Applied Sciences - Stipend, on my work on Wolf Track<br />
            </p>

            <h3>Spoken languages</h3>
                        <p>
            Finnish – native<br />
            English – very good<br />
            Swedish – basics<br />
                        </p>
                            
            <h3>Additional courses</h3>
                        <p>
            08/2006 – 05/2007 	“Computer users A-driving license” (IT Basics, Word, Excel, PowerPoint, MS Access, IE6 Webmail)
                        </p>
                            
            <h3>Skills</h3>
                        <p>
            Photoshop, Gimp, InDesign, Dreamweaver, Illustrator, HTML, CSS, PHP, WordPress, Unity3D, 3ds Max, project managment, scrum, communications, client projects. </p>
            <h3>Programming skills</h3>
                        <p>
            HTML/CSS, PHP – Good<br/>
            JavaScript/jQuery - Basics
                        </p>
            <h3>Hobbies</h3>
                        <p>
            Good movies, good books, playing videogames, jogging, football (both on and off the pitch), tennis, emotional dancing, collecting random stuff and decorating my house to my liking 24/7.
                        </p>

            <h3>Contact me</h3>
            <p>
            email: laurikuparinen [at] protonmail.com<br />
            Find me on <a href="https://www.linkedin.com/in/laurikuparinen/" target="_blank">LinkedIn</a>
            </p>
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>