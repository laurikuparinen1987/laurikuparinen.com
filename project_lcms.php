<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "L*CMS";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>L*CMS</h1>
        <h2>Content Managment System (2008)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>I created my own light-weight Content Managment System for web pages. I coded it in PHP and it doesn't use (and therefore doesn't require) MySQL. The purpose of this project was to have a simple alternative CMS for complex and heavy solutions such as back-then popular Joomla, which I think isn't suitable for small sites.</p>
            <p>The clients that are using L*CMS have been pleased with it's simple and easy-to-use interface where the focus is at editing articles and adding images and links to the articles. "I don't even need any instructions or manual with this thing!", said one of my clients. During our <a href="project_avail.php">Avail</a> times, we used L*CMS (named as AvailCMS) as a lightweight, more accessible and cheaper CMS option for our clients.</p>
            <p>This projects was also my thesis work at EKAMI vocational school back in 2008. My thesis got the highest grade: 5 out of 5.</p>
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>