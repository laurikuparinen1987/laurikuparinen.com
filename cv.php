<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "CV";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>CV</h1>
    </section>

    <section class="content">
        <div class="text-container">
            <p><i>1/2021 -</i><br /><strong>InLove Group Oy, Technical Manager (project Lovena)</strong></p>
            <p class="cvDivider"><a href="https://www.lovena.fi" target="_blank">Lovena</a> is an online dating platform, which combines online dating with developing personal skills. We match our customer with like-minded people (no endless swiping!), help them to make a good first impression and boost their skills to have better flowing conversations. Lovena is changing the whole dating culture by making it less shallow and bringing people together based on their values, lifestyle and future dreams.</p>

            <p><i>11/2020 - 3/2022</i><br /><strong>Buorre, Web developer</strong></p>
            <p class="cvDivider">Web pages for clients, ranging from large ecommerce solutions to small landing pages</p>

            <p><i>05/2017 - 11/2020 </i><br /><strong>Menestystarinat, Web developer</strong></p>
            <p class="cvDivider">At Menestystarinat I coded over 30 websites for clients from start to finish. Site scope varied from small business sites to big catalogue pages. All the pages I did used WordPress and WooCommerce, when applicable. I did also plenty of of SEO (Search Engine Optimization) for our clients, including content optimization and Google Page Speed optimization.</p>

            <p><i>02/2017</i><br /><strong>Web page for Koklaamo</strong></p>
            <p class="cvDivider">Freelancer gig for Kumu Communications.</p>

            <p><i>06/2011 – 11/2014</i><br /><strong><a href="project_avail.php">Avail, designing and programming web pages</a></strong></p>
            <p class="cvDivider">Avail created web pages for small and medium sized companies and registered unions. Avail was founded by me and my friend Marko Palmu and we have worked together on all of Avail's client projects.</p>

            <p><i>9/2014</i><br /><strong>Kymenlaakso University of Applied Sciences, Web programming teacher</strong></p>
            <p class="cvDivider">I was a substitute teacher for a week (8 hours in total) teaching the students about WordPress.</p>

            <p><i>8/2014 – 9/2014</i><br /><strong>Exhibitor at Bilbao, Spain</strong></p>
            <p class="cvDivider">We were promoting Wolf Track game, Microsoft Surface 3 tablets and the city of Kouvola at Bilbao (Spain) for one week during the FIBA Basketball World Cup 2014. We had a stand at Finland supporters hotel, dubbed "Sudenpesä" near downtown Bilbao. Exhibition was done in co-operation with Microsoft of Finland, who gave us then brand-new Surface 3-tablets for the trip. We demonstrated the abilities and power of Surface 3 and also let everyone play Wolf Track on them</p>

            <p><i>12/2013 – 9/2014</i><br /><strong><a href="project_wolftrack.php">Wolf Track video game, producer</a></strong></p>
            <p class="cvDivider">Producer of the game (supervising the production of the game)</p>

            <p><i>10/2013 – 11/2013</i><br /><strong>Planetary Guard: Defender, 3D art trainee</strong></p>
            <p class="cvDivider">I worked on two game projects while working at Blackland Games: KymiRing Racing (Texture Artist, QA) and Planetary Guard: Defender (3D Art Trainee). Both games were made for mobile devices so I had to keep that in mind with texture sizes, polycount etc. so that the games would run well in back-then slightly sluggish touch devices. KymiRing Racing never got released, however Planetary Guard: Defender did and it was available to download from the Google Play Store.</p>

            <p><i>6/2013 – 8/2013</i><br /><strong><a href="project_blackland.php">KymiRing video game, texturing</a></strong></p>
            <p class="cvDivider">While working as an intern at Blackland Games, I worked on on a race track presentation done in Unity3D. I textured nearly the entire scene, including the car and the enviroments. Final project yet to be released.</p>

            <p><i>11/2012 – 02/2013</i><br /><strong>LogistiikkaSeikkailu, 3D-modelling and texturing</strong></p>
            <p class="cvDivider">3D-modelling and texturing in an educational game based on logistics.</p>

            <p><i>11/2012</i><br /><strong>DigiExpo 2012, Exhibitor</strong></p>
            <p class="cvDivider">On behalf of school we were showcasing Awakening game as well as our school's game development program at DigiExpo 2012. I also gave an interview to YLEX during the weekend.</p>

            <p><i>06/2012 – 11/2012</i><br /><strong>Awakening video game, level design, texturing, 3D-modelling</strong></p>
            <p class="cvDivider">3D-modelling, texturing and level design of an unreleased 2D side scrolling game for tablets.</p>

            <p><i>02/2008 – 06/2008</i><br /><strong>AllSpec Oy, customer service and computer maintenance</strong></p>
            <p>Maintenance and building of client's computers.</p>
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>