<!DOCTYPE html>
<html lang="en">

<?php include_once("header.php") ?>  

<body class="index">

    <section class="intro">
        <img src="/img/header.png" alt="Photo of Lauri" />
        <h1>Lauri Kuparinen</h1>
        <h2>Personal portfolio</h2>
    </section>

    <section class="content">
        <div class="index-intro">
            <h3>Wait, who?</h3>
            <p><a href="bio.php">→ Read my Bio</a></p>
            <p><a href="cv.php">→ Check out my CV</a></p>
        </div>

        <h2>Stuff I've done</h2>

        <h3>Some websites I've worked on...</h3>
            <ul>
                <li><a href="https://futurelab.fi/" target="_blank">Futurelab</a> - Programming of the site, with a heavy focus on responsive design (2021)</li>
                <li><a href="https://www.playfinland.fi/" target="_blank">PlayFinland</a> - I planned the design and structure, and finally built it in Webflow (2021)</li>
                <li><a href="https://doorway.fi/" target="_blank">Doorway</a> - Programming of the site. Emphasis of the site was to create as much conversions as possible (2020)</li>
                <li><a href="https://uusieliel.fi/" target="_blank">Uusi Elielinaukio</a> - Architect competition page. Visual layout was done by Sitowise (2020)</li>
                <li><a href="https://neonsun.fi/" target="_blank">Neonsun</a> - Main menu on left, retailer search that could be accessed from a single brand page, emphasis on brand images (2019)</li>
                <li><a href="https://asa-isannointi.fi//" target="_blank">ASA Isännöinti</a> - First massive website I worked on. Graphically this turned out fine (2017)</li>
            </ul>

        <h3>Video Game Projects</h3>
            <ul>
                <li><a href="project_smbhack.php">Lauri's Hack (Super Mario Bros. Hack) - Level design and ROM hacking</a> (2019)</li>
                <li><a href="http://gamejolt.com/games/sam-doesn-t-save-the-world/39493" target="_blank">Sam doesn't save the World - Producer and UI programming</a> (2014)</li>
                <li><a href="project_wolftrack.php">Wolf Track - Producer</a> (2014)</li>
                <li><a href="project_blackland.php">KymiRing Racing - Texture artist</a> (2013)</li>
                <li>HeXart - Programming (2013)</li>
                <li>Logistics Adventure - 3D modelling and texturing (2012)</li>
                <li>Awakening - 3D modelling, texturing and level building (2012)</li>
                <li><a href="project_vicecity.php">Chas Tenenbaum skin for GTA: Vice City</a> (2009)</li>
                <li><a href="project_mots.php">Mysteries of the Sith Mod - UI programming</a> (2009)</li>
                <li><a href="project_pes.php">Pro Evolution Soccer 6 team kit textures</a> (2007)</li>
            </ul>

        <h3>The Uncategorizables</h3>
            <ul>
                <li>Winner of GamingOnLinux.com Desktop wallpaper competition (2016)</li>    
                <li>Voluntary work at Toivontori, Lempäälä (2015)</li>
                <li>Wolf Track and Microsoft Surface 3 showcasing at Bilbao, Spain - Exhibitor (2014)</li>
                <li><a href="project_riverofslime.php">River of Slime Unity3D scene - Modelling, texturing, level design</a> (2012)</li>
                <li>Awakening and KyAMK advertising at DigiExpo - Exhibitor (2012)</li>
                <li><a href="project_media.php">Media appearances and speeches throughout the years (2012-)</a></li>
                <li><a href="project_ktp.php">KTP Database - Football Club statistics (2006-)</a></li>
            </ul>
    
    </section>

<?php include_once("footer.php") ?>
</body>
</html>