<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Wolf Track";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Wolf Track</h1>
        <h2>Producer of the game (2014)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>Wolf Track is a cross-platform mobile game featuring Finnish national basketball team in an adventure encouraging the kids to play basketball. The game is a joint effort between the Finnish Basketball Association and Kymenlaakso University of Applied Sciences (KyAMK) Game Design program.</p>

            <p>My responsibilities on the project were the following:</p>

            <p>PRODUCING AND GAME DESIGN</p>
            <ul>
            <li>Producer of the game (supervising the production of the game)</li>
            <li>Research on publishing the game to different platforms, such as technical requirements and digital store requirements</li>
            <li>Development of patches with the main programmer after the game was released</li>
            <li>Some game design on the project, mostly suggesting changes and coming up with some ideas that may have ended up in the final game</li>
            <li>Compiling the assets and uploading the game to digital stores on various platforms (AppStore, Google Play, Win 8.1 Marketplace)</li>
            </ul>

            <p>PROJECT MANAGEMENT</p>
            <ul>
            <li>Timing the project from the beginning to the end and assigning duties to co-workers (scrumming)</li>
            <li>Held lessons to co-workers about the usage of SVN Tortoise and graphic asset production</li>
            <li>Arranging game testing sessions with the help of client</li>
            </ul>

            <p>ARRANGEMENTS AND CONTACTS</p>
            <ul>
            <li>Scheduling meetings</li>
            <li>Co-operation with the teachers of KyAMK and other personnel on the project</li>
            <li>Co-operation with the client (Finnish Basketball Association) on a near daily basis concerning issues such as licensing, the content of the game, permissions, release, the script etc.</li>
            <li>Contacting the Kouvola area elementary schools for game testing with the children</li>
            <li>Co-operation with Microsoft of Finland regarding the WPhone and Win 8.1 development of the game</li>
            <li>Took care of contracts, student credits etc. for the co-workers</li>
            <li>Recruiting additional workforce for the project using my personal contacts</li>
            </ul>

            <p><a href="https://www.microsoft.com/fi-fi/store/p/wolf-track/9wzdncrdlp7b" target="_blank">Download Wolf Track (MS Store)</a> for your MS Windows 8.1 + devices.
            </p>
        </div>

        <div class="image-container">
            <img src="projects/wolftrack/keyart-preview.jpg" alt="Keyart">
            <img src="projects/wolftrack/ingame-preview.jpg" alt="Ingame screenshot">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>