<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Kymi Ring Racing";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>KymiRing Racing</h1>
        <h2>Texture artist (2013)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>While working as an intern at Blackland Games, I worked on a race track presentation done in Unity3D. I textured nearly the entire scene, including the car and the enviroments. Final project yet to be released (and probably never will...). Check out the trailer for the game below and see what could have been:</p>
            
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/SIoXxs6F7IU"></iframe>
            </div>
                            
            <p>Here is some of my texture work for the game:</p>
        </div>

        <div class="image-container">
            <img src="projects/blackland/car-inside-preview.png" alt="Car interior">
            <img src="projects/blackland/car-preview.png" alt="Car exterior">
            <img src="projects/blackland/objects-preview.png" alt="Trackside objects">
            <img src="projects/blackland/surfaces-preview.png" alt="Surfaces">
        </div>

        <div class="text-container">
            <p>While working at Blackland Games, I also worked on a mobile game called Planetary Guard: Defender as a 3D trainee. You can download the game for your Android device from <a href="https://play.google.com/store/apps/details?id=com.blacklandgames.pg&hl=fi">here</a>.</p>
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>