<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Mysteries of the Sith Mod";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Mysteries of the Sith</h1>
        <h2>UI coding in Total Conversion Mod (2009)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>Mysteries of the Sith was a total conversion modification (a mod) for a video game called Star Wars: Jedi Knight: Jedi Academy (2003). Mysteries of the Sith was a video game for PC originally released by LucasArts in 1998 and our team's goal was to recreate it using a more modern engine (in this game's case a modded Quake III Arena engine).</p>
            <p>My main responsibility was to recreate the original game's menu system for the mod. The project leaders of the mod wanted the UI to look, work and feel exactly like the original so I basically had to recode the entire menu structure found in Jedi Academy (which is vastly different from Mysteries of the Sith's menu).</p>
            <p>The UI code in Jedi Academy itself is C -like language in which you define where elements are, how they look, sound and what they do when pressed. The process was really difficult because back in 2009, Raven Software (the makers of Jedi Academy) had not released the single player source code of the game so I had to come up with all sorts of workarounds for the UI-code to do the stuff I wanted to. For example, the Force Power selection menu and the option to select Widescreen screen modes was really hard to pull off but I eventually came up with ways to do both of them.</p>
            <p>Unfortunately the mod died because the workload was too much for our small team. The UI part was the only thing that got made while the others made some models, a couple of half-ready levels and that's about it. You can see old W.I.P. video of the menu <a href="http://www.moddb.com/mods/mysteries-of-the-sith-jka-modification/videos" target="_blank">here!</a></p>
            <p>You can download and examine the source code of the UI <a href="https://github.com/laurikuparinen1987/jka_mots_ui" target="_blank">at GitHub</a>.</p>
        </div>

        <div class="image-container">
            <img src="projects/mots/new-menu-preview.jpg" alt="New menu">
            <img src="projects/mots/pause-menu-preview.png" alt="New menu">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>