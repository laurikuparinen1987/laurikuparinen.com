<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Pro Evolution Soccer textures";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Pro Evolution Soccer 6 textures</h1>
        <h2>Finnish Veikkausliiga team kits</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>Quite often I've tried to find ways to combine my interest in games and technology and my interest for football. Usually this has meant that I've modded either PES or FIFA game textures and created new kits, adboards, faces, stadiums etc.. By far my best work in football games has been team kit textures. My personal highlight of team kits are some of the Finnish Veikkausliiga kits for Pro Evolution Soccer 6 (game from the year 2006).    
            </p>
            <p>Below are some examples of the kit textures I created. Obviously, I first made kits for my favourite club KTP (then known as KooTeePee). RoPS kit was a request from a football forum member and FC Viikingit kit turned out fine, hence it's inclusion below.</p>
            <p>Full resolution of the kit is 512x256 pixels; PC version of PES 6 is really just a straight PlayStation 2 port so that explains the low resolution of the textures. It was a bit difficult to put so much detail (and kit adverts) on those kits, keep them readable and have the proportions right!</p>
        </div>

        <div class="image-container">
            <img src="projects/pes/ktp.png" alt="KTP football kit">
            <img src="projects/pes/ktp_mv.png" alt="KTP football kit">
            <img src="projects/pes/rops.png" alt="RoPS football kit">
            <img src="projects/pes/viikingit.png" alt="Viikingit football kit">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>