<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Vice City retro Adidas suit";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Retro adidas texture</h1>
        <h2>GTA Vice City skin (2009)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>Okay, I know, this is a weird one: a Grand Theft Auto Vice City PC skin inspired by the movie <a href="https://www.imdb.com/title/tt0265666/" target="_blank">The Royal Tenenbaums</a>. More specifically, the skin is based on Ben Stiller's characters (Chas Tenenbaum) outfit: a classic 80's adidas tracksuit. It's difficult to think two things more apart from each other: movies by Wes Anderson and Grand Theft Auto games. But here they are, together at last. I thought the red adidas retro track suit would fit into a game like Vice City (set in the 80's) perfectly. And I think it does.
            </p>

            <p>The challenge here was to modify the base Tommy Vercetti skin file so I could skin the red tracksuit on it. Unfortunately, the shirt proved to be a problem. I couldn't place the classic adidas logo on it, because it would get copied all over the shirt. Same thing with the adidas stripes running along the sleeves: couldn't be done. So what you see here is - what I think - the best I could do given the limitations of the skin. It fits nicely with Vice City's artistic style, and you still get the adidas stripes on the trousers and adidas sneakers. This texture is mostly an example on how to deal with limitations and do the best work possible with a set of limitations on mind. I don't want to brag, but I think I'm really good at dealing with techincal limitations and overcoming them.
            </p>
        </div>

        <div class="image-container">
            <img src="projects/vicecity/stiller.jpg" alt="Ben Stiller in The Royal Tenenbaums">
            <img src="projects/vicecity/2.jpg" alt="Tommy Vercetti wearing red adidas tracksuit">
            <img src="projects/vicecity/tommy-adidas2.bmp" alt="Vice City skin">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>