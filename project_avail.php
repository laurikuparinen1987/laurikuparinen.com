<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Avail";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Avail</h1>
        <h2>Small website company with a friend (2011-2014)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>Home pages for small and medium sized companies and registered unions. Avail was founded by me and my friend Marko Palmu and we worked together on all of Avail's client projects. Some of my site designs I did for the clients can be found on <a href="project_weblayouts.php">website layouts subpage</a>.</p>
        </div>

        <div class="image-container">
            <img src="projects/avail/kotisivut2014-preview.png" alt="Homepage">
        </div>
    </section>

<?php include_once("footer.php") ?>
</body>