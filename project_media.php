<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Media appearances";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Media appearances & Speeches</h1>
        <h2>Media appearances and public speeches (2012-)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>All of my media appearances in one convenient list:</p>
            <ul>
                <li><a href="https://www.pkank.fi/uutiset/harvalla-tarkkoja-ulkonakokriteereita-6.19.27731.843d09721c" target="_blank">Newspaper: Har­val­la tark­ko­ja ul­ko­nä­kök­ri­tee­rei­tä (2023)</a> - Interview on Lovena and dating - Kaupunkilehti Ankkuri</li>
                <li><a href="https://www.mtv.fi/sarja/uutisaamu-33001003008/nettideittailu-sen-oikean-etsimista-vai-pinnallista-bisnesta-1486808" target="_blank">TV: Nettideittailu: Sen Oikean etsimistä vai pinnallista bisnestä? (2021)</a> - Interview on dating and Lovena - MTV3</li>
                <li><a href="https://www.instagram.com/p/CI6JIAsJMON/" target="_blank">IG: Nettideittailu miehen näkökulmasta (2020)</a> - Live talk on mens dating with Katri Gruner from Match by K</li>
                <li><a href="https://kymensanomat.fi/uutiset/urheilu/af32ba76-6fcd-4a5f-9ea0-576cae7281ee" target="_blank">Newspaper: Tilastot kertovat tarinoita (2020)</a> - Interview on my KTP database hobby - Kymen Sanomat</li>
                <li><a href="https://radio.uta.fi/huuhkajat-ja-taustaparvi-lentavat-samassa-aurassa/?fbclid=IwAR1lhIlnN6eDJwCCpm_zgf6cjbQ0zDKKkG3zHoxrrWIpiWmcTTzR6aHmp9I" target="_blank">Radio: Huuhkajat ja taustaparvi lentävät samassa aurassa (2016)</a> - Interview on my Huuhkajat supporting career - Radio Moreeni</li>
                <li>Theater: Työtä päin! (2014) - I was interviewed for the Kouvola City Theatre documentary play on video game development; an actress (yes, a woman!) played me in the play </li>
                <li>TV: Matkalla MM-kisoihin #Susijengi (2014) - Interview on Wolf Track video game - YLE TV2</li>
                <li><a href="https://www.youtube.com/watch?v=jmF00aqXnd8&t=57s" target="_blank">YouTube: Lauri Kuparinen & Wolf Track (2014)</a> - Interview on Wolf Track video game</li>
                <li><a href="https://yle.fi/aihe/artikkeli/2012/11/03/suomalaiset-oppilaitokset-heraavat-kasvavan-pelialan-tarpeisiin-hautuuko-taalla" target="_blank">Radio: Interview on game development (2012) - YLEX </a></li>
                <li>TV: I met Paavo Arhinmäki during his presidential campaign; my clumsy small talk with him ended up on a tv program called 'Ajankohtainen Kakkonen'. A highlight of my media career (2012) - YLE TV2 </li>
            </ul>

            <p>Public speeches:</p>
            <ul>
                <li><a href="https://www.trestart.fi/wp-content/uploads/2018/12/TalentShare-Oman-portfolion-rakentaminen-Kuparinen.pdf" target="_blank">Oman portfolion rakentaminen</a> - A speech on creating personal portfolio at TreStart (2016)</li>
                <li>Developing Wolf Track - A speech in front of the faculty members of Kymenlaakso University of Applied Sciences during the school season opening ceremony (2014)</li>
                <li>Developing Awakening - A speech on game development at the opening ceremony of KyAMK game studios (2012)</li>
            </ul>
        </div>


    </section>

<?php include_once("footer.php") ?>
</body>