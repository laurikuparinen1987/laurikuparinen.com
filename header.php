<head>
	<title>LauriKuparinen.com <?php if ($title_extension) { echo "| " . $title_extension; } ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Portfolio of Lauri Kuparinen">
    <meta name="author" content="Lauri Kuparinen">
    
    <link rel="icon" href="favicon.ico">
	<link rel="stylesheet" href="/css/style.css">

</head>