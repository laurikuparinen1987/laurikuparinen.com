<!DOCTYPE html>
<html lang="en">

<?php 
    $title_extension = "Lauri's hack";
    include_once("header.php")
?>  

<body class="project">

<?php include_once("navbar.php") ?>  

    <section class="intro">
        <img src="/img/ornament-small.png" class="ornament" alt="Ornament" />
        <h1>Lauri's Hack</h1>
        <h2>Super Mario Bros. Hack (2019)</h2>
    </section>

    <section class="content">
        <div class="text-container">
            <p>Lauri’s Hack is a rom hack of the original Nintendo Entertaiment System game Super Mario Bros (1985). I tried to create Super Mario Bros. levels that feel similar to Super Mario Bros. and Super Mario Bros.: The Lost Levels (the Japanese Super Mario Bros 2.). Difficulty wise this hack falls somewhere between those two; this hack shouldn’t be as hard as The Lost Levels is but it still should provide a good challenge.</p>

            <p style="text-align: center;">A review of my hack (from <a href="https://www.romhacking.net/reviews/4570/#review" target="_blank">Romhacking.net</a>):</p>
            <blockquote>"Just as the title says, I’d say this is a great hack to play through inbetween the original SMB1 and SMB2 The Lost Levels. The level designs are pretty unique and lore-fitting, so if you’d like to play a vanilla looking hack, feel free to try this one out! :D" <br />- Jeansowaty, 30 Aug 2019</blockquote>

            <p><strong>Latest version (2.0 beta)</strong> with updated sprites, palette and small level adjustments is available <a href="projects/smbhack/LaurisHack_gfx.ips">here</a>.</p>

            <p><strong>The original 1.0 version</strong> download link is available at <a href="https://www.romhacking.net/hacks/4479/" target="_blank">Romhacking.net</a>. To play this hack, you will need an NES emulator (or original hardware with <a href="https://krikzz.com/store/home/31-everdrive-n8-nes.html" target="_blank">Everdrive cassette</a>), original Super Mario Bros. rom image (PAL version) and an IPS patcher.</p>
        </div>

        <div class="image-container">
            <img src="projects/smbhack/5.png" alt="Screenshot of an updated version with sprites made by my brother">
            <img src="projects/smbhack/4.png" alt="Screenshot of an updated version with sprites made by my brother">
            <img src="projects/smbhack/2.png" alt="Screenshot of level 1-1">
            <img src="projects/smbhack/3.png" alt="Screenshot of level 1-3">
        </div>

    </section>

<?php include_once("footer.php") ?>
</body>